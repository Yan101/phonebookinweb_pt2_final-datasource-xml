﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    DAL dataAccessLayer;
    DataSet personDataSet;
    TextBox[] headerTextBoxes;
    TextBox[] filteredTextBoxes;

    protected void Page_Load(object sender, EventArgs e)
    {
        //SqlDataSource1.DeleteCommand = "Delete From Person Where (id = @id)";

        this.headerTextBoxes = new TextBox[]
        {
            this.TextBox0, this.TextBox1, this.TextBox2, 
            this.TextBox3, this.TextBox4, this.TextBox5, 
            this.TextBox6, this.TextBox7, this.TextBox8
        };

        foreach (TextBox tb in this.headerTextBoxes)
            tb.Enabled = false;

        this.filteredTextBoxes = new TextBox[]
        {
            nameTB, surNameTB, countryTB, sityTB, streetTB, houseTB
        };

        //присвоить все textBox = св-во AutoPostBack и подписать на событие
        foreach (TextBox tb in filteredTextBoxes)
        {
            tb.AutoPostBack = true;
            tb.TextChanged += tb_TextChanged;
        }

        //******Повесили XmlDataSource***
        /*
            this.dataAccessLayer = new DAL();
            this.personDataSet = this.dataAccessLayer.GetAllPersons(); //получили занные.

            //нужно задать первичный ключ для совершения поиска и дальнейшего РЕДАКТИРОВАНИЯ строки.
            //PrimaryKey принимает DataColumn[], поэтому нужно создать массив ключей (т.к.несколько ключей могут быть первичными ключами)
        
            this.personDataSet.Tables[0].PrimaryKey = new DataColumn[] {this.personDataSet.Tables[0].Columns["id"]};

            this.phoneGridView.DataSource = this.personDataSet; //GridView - узнал где лежат данные
            this.phoneGridView.DataBind(); //GridView - теперь возьмет данные.
        */
        //******Повесили XmlDataSource***
    }

    //метод ля события и для поиска записей.
    void tb_TextChanged(object sender, EventArgs e)
    {
        string[] columnName = {"FirstName", "LastName", "Country", "City", "Street", "House"};
        DataView filteredPerson = null;
        string filter = null;

        for (int i = 0; i < this.filteredTextBoxes.Length; i++)
        {
            if (i > 0)
                filter += " AND ";
            
            filter += columnName[i] + " like '%" + this.filteredTextBoxes[i].Text.Trim() + "%'";
            
            filteredPerson = this.personDataSet.Tables[0].AsDataView(); //DATAVIEW!!!
            filteredPerson.RowFilter = filter; //убираем ненужные!
        }

        this.phoneGridView.DataSource = filteredPerson;
        this.phoneGridView.DataBind();
    }

    //строка будет выделенна и потом можно доступится к Ячейкам!
    protected void phoneGridView_SelectedIndexChanged(object sender, EventArgs e)
    {
        /*
        int i = 0;
        foreach (TableCell  column  in this.phoneGridView.SelectedRow.Cells)8
        {
            if(i>=1)
          {
                if (column.Text == "&nbsp;")
                    this.headerTextBoxes[i - 1].Text = "нет данных";
                else
                    this.headerTextBoxes[i - 1].Text = column.Text;
            }
            i++;
        }
        */
    }

    //Тут получим прежнюю SelectedRow.
    //Это собтие вызывается до установки gridView в новую строку.
    //Выделенна прежняя строка, но данные а том какую хотим выделить - уже есть, это GridViewSelectEventArgs e
    protected void phoneGridView_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        int i = 0;
        //строка еще не выделенна, поэтому доступится нужно сначала к строке!
        foreach (TableCell column in this.phoneGridView.Rows[e.NewSelectedIndex].Cells)
        {
            if (i >= 3)
            {
                if (column.Text == "&nbsp;")
                    this.headerTextBoxes[i - 3].Text = "нет данных";
                else
                    this.headerTextBoxes[i - 3].Text = column.Text;
            }
            i++;
        }
    }
    protected void showAllbtn_Click(object sender, EventArgs e)
    {
        for (int i = 0; i < this.filteredTextBoxes.Length; i++)
            this.filteredTextBoxes[i].Text = string.Empty;

        this.phoneGridView.DataSource = this.personDataSet;
        this.phoneGridView.DataBind();
    }

    protected void EditButton_Click(object sender, EventArgs e)
    {
        GoToEditMode();
        this.actionHiddenField.Value = Action.EditPerson.ToString();
        this.EditButton.Visible = true;
    }
    protected void AddButton_Click(object sender, EventArgs e)
    {
        GoToEditMode();
        this.actionHiddenField.Value = Action.AddPerson.ToString();

        foreach (TextBox tb in this.headerTextBoxes)
            tb.Text = string.Empty;

        DataRow newPersonRow = this.personDataSet.Tables[0].NewRow(); //получили ссылку на новую строку!!!

        newPersonRow["id"] =
             int.Parse(this.personDataSet.Tables[0].Rows[this.personDataSet.Tables[0].Rows.Count - 1]["id"].ToString()) + 1;
        headerTextBoxes[0].Text = newPersonRow["id"].ToString();

        /////////////////////////////
        this.AddButton.Visible = true;
    }
    protected void DeleteButton_Click(object sender, EventArgs e)
    {
        GoToEditMode();
        this.actionHiddenField.Value = Action.DeletePerson.ToString();
        this.headerTextBoxes[0].ForeColor = System.Drawing.Color.Red;

        this.DeleteButton.Visible = true;
    }

    //Начало редактирования
    private void GoToEditMode()
    {
        foreach (TextBox tb in this.headerTextBoxes)
            tb.Enabled = true;
        headerTextBoxes[0].Enabled = false;

        //this.savePersonPanel.Visible = true;
        this.saveButton.Visible = true;
        this.cancelButton.Visible = true;

        this.EditButton.Visible = false;
        this.AddButton.Visible = false;
        this.DeleteButton.Visible = false;
    }

    //конец редактирования
    private void EndEditMode()
    {
        foreach (TextBox tb in this.headerTextBoxes)
            tb.Enabled = false;

        //this.savePersonPanel.Visible = false;
        this.saveButton.Visible = false;
        this.cancelButton.Visible = false;

        this.EditButton.Visible = true;
        this.AddButton.Visible = true;
        this.DeleteButton.Visible = true;
    }

    protected void cancelButton_Click(object sender, EventArgs e)
    {
        EndEditMode();
        foreach (TextBox tb in this.headerTextBoxes)
            tb.Text = string.Empty;

    }

    protected void saveButton_Click(object sender, EventArgs e)
    {
        this.EndEditMode(); //вернутся в обычный режим в любом случае

        if (this.actionHiddenField.Value == Action.AddPerson.ToString())
        {
            DataRow newPersonRow = this.personDataSet.Tables[0].NewRow(); //получили ссылку на новую строку!!!
            
            //Сделать +1 от той записи, которая предшевствует данной! А предшевствует данной записи, пока что последняя, НО в будующем она станет предпоследней
            //this.personDataSet.Tables[0].Rows[ ИНДЕКС последней строки ]
            //[this.personDataSet.Tables[0].Rows.Count - 1] - последняя строка
            //["id"] - берем значение столбца из этой строки!
            //Мы получили значение из последней строки и теперь добавим к новому ID + 1
            newPersonRow["id"] =
                int.Parse(this.personDataSet.Tables[0].Rows[this.personDataSet.Tables[0].Rows.Count - 1]["id"].ToString()) + 1;
//            headerTextBoxes[0].Text = newPersonRow["id"].ToString();


            //устанвоим значение всех остальных ячеек!
            //пройтись по колонкам нашей table данной Row и заносить туда значения из headerTextBoxes!
            //пробегаюясь по массиву headerTextBoxes - можно пробегатся параллельно по колонкам данной строки или наоборот
            for (int i = 1; i < this.personDataSet.Tables[0].Columns.Count; i++)
                newPersonRow[i] = this.headerTextBoxes[i].Text.Trim();

            //добавить строку в DataSet
            this.personDataSet.Tables[0].Rows.Add(newPersonRow);
        }

        if (this.actionHiddenField.Value == Action.EditPerson.ToString())
        {
            //получить данную строку для редактирования.
            //Для метода Find задали PrimaryKey
            DataRow editPersonRow = this.personDataSet.Tables[0].Rows.Find(this.headerTextBoxes[0].Text);

            //см описание выше!
            for (int i = 0; i < this.personDataSet.Tables[0].Columns.Count; i++)
                editPersonRow[i] = this.headerTextBoxes[i].Text.Trim();
        }

        if (this.actionHiddenField.Value == Action.DeletePerson.ToString())
        {
            //Находим строку и выполняем метод Delete()
            this.personDataSet.Tables[0].Rows.Find(this.headerTextBoxes[0].Text).Delete();
        }

        //Измененный DataSet передаем в DAL для сохранения
        this.dataAccessLayer.SavePersonsToDB(this.personDataSet);
        this.personDataSet.AcceptChanges(); //для профилактики.

        //перезагрузить GridView из файла, новыми данными
        this.phoneGridView.DataBind();
    }

    //привязка данных ДГВ, по одной строкег(событие возникает, когда создаетсястрока и наполняется данными).э
    protected void phoneGridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow) //текущая строка == 
        {
            e.Row.Attributes["onmouseover"] = "this.style.backgroundColor='Red'"; 
            if (e.Row.RowIndex % 2 == 0)
                e.Row.Attributes["onmouseout"] = "this.style.backgroundColor='#f6f6f6'";
            else
                e.Row.Attributes["onmouseout"] = "this.style.backgroundColor='white'";
        }

    }

}