﻿CREATE PROCEDURE [dbo].UpdatePersonStoredProc
	@id int,
	@FirstName nvarchar(50),
	@LastName nvarchar(50),
	@Country nvarchar(50),
	@City nvarchar(50),
	@Street nvarchar(50),
	@House int,
	@Phone1 nvarchar(50),
	@Phone2 nvarchar(50),
	@isAdmin bit
AS
	UPDATE Person SET FirstName = @FirstName, LastName = @LastName, Country = @Country, City = @City, Street = @Street, House = @House, Phone1 = @Phone1, Phone2 = @Phone2, isAdmin = @isAdmin WHERE (id = @id);
RETURN