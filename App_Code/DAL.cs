﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

public class DAL
{
    private string path = System.Web.HttpContext.Current.Server.MapPath("~\\App_Data\\PersonsDB.xml");
	public DAL(){   }

    public DataSet GetAllPersons()
    {
        DataSet persons = new DataSet();
        persons.ReadXml(path);
        return persons;
    }

    public void SavePersonsToDB(DataSet ds)
    {
        ds.WriteXml(this.path);
    }
}