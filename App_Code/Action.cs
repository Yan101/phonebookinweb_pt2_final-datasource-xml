﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Action
/// </summary>
public enum Action
{
    AddPerson,
    EditPerson,
    DeletePerson
}