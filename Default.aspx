﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Phone Book</title>
</head>
<body>
    <form id="mainForm" runat="server">
    <div>
        <asp:Panel runat="server" ID="HeaderPanel" Width="850px" >
            <asp:Table runat="server" HorizontalAlign="Justify" Width="850px">
                <asp:TableHeaderRow BackColor="#CCCC00" runat="server"><asp:TableHeaderCell runat="server" ColumnSpan="4">PHONE BOOK</asp:TableHeaderCell></asp:TableHeaderRow>
                    
                    <asp:TableRow runat="server" HorizontalAlign="Right">
                        <asp:TableCell runat="server">ID<asp:TextBox ID="TextBox0" Width="20" runat="server" ReadOnly="True"></asp:TextBox>&nbsp;Имя<asp:TextBox ID="TextBox1" runat="server" Width="40"></asp:TextBox></asp:TableCell>
                        <asp:TableCell runat="server">Фамилия<asp:TextBox ID="TextBox2" runat="server" Width="100"></asp:TextBox></asp:TableCell>
                        <asp:TableCell runat="server">Страна<asp:TextBox ID="TextBox3" runat="server" Width="100"></asp:TextBox></asp:TableCell>
                        <asp:TableCell runat="server">Город<asp:TextBox ID="TextBox4" runat="server" Width="100"></asp:TextBox></asp:TableCell>
                    </asp:TableRow>
                
                    <asp:TableRow runat="server" HorizontalAlign="Right">
                        <asp:TableCell runat="server">Улица<asp:TextBox ID="TextBox5" runat="server" Width="70"></asp:TextBox></asp:TableCell>
                        <asp:TableCell runat="server">Дом<asp:TextBox ID="TextBox6" runat="server" Width="100"></asp:TextBox></asp:TableCell>
                        <asp:TableCell runat="server">Тел1<asp:TextBox ID="TextBox7" runat="server" Width="100"></asp:TextBox></asp:TableCell>
                        <asp:TableCell runat="server">Тел2<asp:TextBox ID="TextBox8" runat="server" Width="100"></asp:TextBox></asp:TableCell>
                    </asp:TableRow>

                    <asp:TableFooterRow BackColor="#339966" runat="server">
                        <asp:TableHeaderCell runat="server" ColumnSpan="4">
                            <asp:LinkButton runat="server" ID="EditButton" OnClick="EditButton_Click" ForeColor="White">Редактировать</asp:LinkButton>&nbsp;
                            <asp:LinkButton runat="server" ID="AddButton" OnClick="AddButton_Click" ForeColor="White">Добавить</asp:LinkButton>&nbsp;
                            <asp:LinkButton runat="server" ID="DeleteButton" OnClick="DeleteButton_Click" ForeColor="White">Удалить</asp:LinkButton>&nbsp;
                        </asp:TableHeaderCell>
                    </asp:TableFooterRow>
            </asp:Table>
            
          <!-->  <asp:Panel runat="server" ID="savePersonPanel" HorizontalAlign="Center" Visible="true" Width="850px">
                <asp:HiddenField ID="actionHiddenField" runat="server" />
              <!-->
                <asp:Button ID="saveButton" runat="server" Text="Сохранить" OnClick="saveButton_Click" Visible="False"/>&nbsp;
                <asp:Button ID="cancelButton" runat="server" Text="Отменить" OnClick="cancelButton_Click" Visible="False"/>
              <!-->
            </asp:Panel>
        </asp:Panel>
              <!-->
                <asp:XmlDataSource ID="XmlDataSource1" runat="server" DataFile="~/App_Data/PersonsDB.xml" TransformFile="~/App_Data/ConvertXML.xsl"></asp:XmlDataSource>

        <asp:Panel runat="server" HorizontalAlign="Center" Width="850px">
            &nbsp;Имя<asp:TextBox ID="nameTB" runat="server" Width="60px"></asp:TextBox>
            &nbsp;Фамилия<asp:TextBox ID="surNameTB" runat="server" Width="60"></asp:TextBox>
            &nbsp;Страна<asp:TextBox ID="countryTB" runat="server" Width="60"></asp:TextBox>
            &nbsp;Город<asp:TextBox ID="sityTB" runat="server" Width="60"></asp:TextBox>
            &nbsp;Улица<asp:TextBox ID="streetTB" runat="server" Width="60"></asp:TextBox>
            &nbsp;Дом<asp:TextBox ID="houseTB" runat="server" Width="60"></asp:TextBox>
            &nbsp;<asp:Button runat="server" ID="showAllbtn" Text="Сброс поиска" OnClick="showAllbtn_Click"/>
        </asp:Panel>
            
        <asp:Panel runat="server" ID="MainPanel" ScrollBars="Auto" Height="300px" HorizontalAlign="Justify" Width="850px" >
            
            <asp:GridView ID="phoneGridView" runat="server" CellPadding="4" ForeColor="#333333" 
                GridLines="None" AutoGenerateSelectButton="True" 
                OnSelectedIndexChanged="phoneGridView_SelectedIndexChanged" 
                OnSelectedIndexChanging="phoneGridView_SelectedIndexChanging" 
                HorizontalAlign="Center" OnRowDataBound="phoneGridView_RowDataBound" 
                DataSourceID="SqlDataSource1" AutoGenerateColumns="False" Width="821px" 
                AllowPaging="True" PageSize="5" AllowSorting="True" DataKeyNames="id" >
                <AlternatingRowStyle BackColor="White" />
                <Columns>
                    <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                    <asp:BoundField DataField="id" HeaderText="id" SortExpression="id" InsertVisible="False" ReadOnly="True" />
                    <asp:BoundField DataField="FirstName" HeaderText="FirstName" SortExpression="FirstName" />
                    <asp:BoundField DataField="LastName" HeaderText="LastName" SortExpression="LastName" />
                    <asp:BoundField DataField="Country" HeaderText="Country" SortExpression="Country" />
                    <asp:BoundField DataField="City" HeaderText="City" SortExpression="City" />
                    <asp:BoundField DataField="Street" HeaderText="Street" SortExpression="Street" />
                    <asp:BoundField DataField="House" HeaderText="House" SortExpression="House" />
                    <asp:BoundField DataField="Phone1" HeaderText="Phone1" SortExpression="Phone1" />
                    <asp:BoundField DataField="Phone2" HeaderText="Phone2" SortExpression="Phone2" />
                    <asp:CheckBoxField DataField="isAdmin" HeaderText="isAdmin" SortExpression="isAdmin" />
                </Columns>
                <EditRowStyle BackColor="#7C6F57" />
                <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#E3EAEB" />
                <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#F8FAFA" />
                <SortedAscendingHeaderStyle BackColor="#246B61" />
                <SortedDescendingCellStyle BackColor="#D4DFE1" />
                <SortedDescendingHeaderStyle BackColor="#15524A" />
            </asp:GridView>

            <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
                
                SelectCommand="SELECT id, FirstName, LastName, Country, City, Street, House, Phone1, Phone2, isAdmin FROM Person ORDER BY id, FirstName, LastName" 
                
                UpdateCommand="UpdatePersonStoredProc"
                UpdateCommandType="StoredProcedure"
                
                DeleteCommand="DELETE FROM Person WHERE (id = @id)" 
                
                InsertCommand="INSERT INTO Person(FirstName, LastName, Country, City, Street, House, Phone1, Phone2, isAdmin) VALUES (@FirstName, @LastName, @Country, @City, @Street, @House, @Phone1, @Phone2, @isAdmin)">
                <DeleteParameters>
                    <asp:Parameter Name="id" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="FirstName" />
                    <asp:Parameter Name="LastName" />
                    <asp:Parameter Name="Country" />
                    <asp:Parameter Name="City" />
                    <asp:Parameter Name="Street" />
                    <asp:Parameter Name="House" />
                    <asp:Parameter Name="Phone1" />
                    <asp:Parameter Name="Phone2" />
                    <asp:Parameter Name="isAdmin" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:Parameter Name="id" />
                    <asp:Parameter Name="FirstName" />
                    <asp:Parameter Name="LastName" />
                    <asp:Parameter Name="Country" />
                    <asp:Parameter Name="City" />
                    <asp:Parameter Name="Street" />
                    <asp:Parameter Name="House" />
                    <asp:Parameter Name="Phone1" />
                    <asp:Parameter Name="Phone2" />
                </UpdateParameters>
            </asp:SqlDataSource>

        </asp:Panel> 
    </div>
    </form>
</body>
</html>
